############################################################################################################################
# INTRODUCTION
############################################################################################################################
# This project is part of FAIRwissenschaft project (see FAIRwissenschaft_README.md file for details)  
# title: "RansackAI"
# author: "Shradha Mukherjee"
# status: work in progress (ongoing project)
# date created: "October 1, 2023"
# date last updated: "March 3, 2024"
# submission: Initial Submission "October 11, 2023"  

RansackAI is a computational pipeline to pick candidates for any type of selection for education, job or funding. This scoring system will help pick out talented high performers over untalented poor performers. 
To take analogy from biology host-parasite relationship, this scoring system will protect the host and enable it to function at maximum potential by seperating them from parasites. Criteria used will be as follows (this is not an exhaustive list, more factors may be added during the project):  
1) Grades. It would be useful to have rank of the student and total students (because low score could still be high rank), but usually this is not provided by universities but it would be useful. Grade and Grade_rank would be analyzed as seperate factors.  
2) Publication journal impact factor  
3) Publication major authorhsip.  
4) Publication number of total authors (fewer authors indicates greated individual contributon)  
5) Publication frequency  
6) Publication novelty (comparison with existing literature)  
7) Publication interdiciplinary or not (interdisciplinary work will get more points)  
8) Growth of individual score by tracking their funding and projects where they are major author (doing same work for many years never completing anything indicates stagnation so less score)  
9) Standardised test scores  
10) Awards, funds and positions/jobs are scored.  

RansackAI will ignore or negatively score 'woke' category based applications. These parasites will also be identified by detecting with AI-NLP parasites who use 'woke' category/quota to secure education/funding/jobs. Giving this 'woke' category/quote favorable points is discrimination against talent and not hiring talent in an effort to hire 'woke' candidates is loss, and liability for organization. Any mention of 'woke' categories/quotes and synonyms, such as black/afro-american, african, hispanic, latino, lgbtqrstuvwx, caste, aboriginal, and other caste, race, religion, gender, orientation, refugee status and illegal immigration status (this last one is a criminal breaking immigration laws so automatically disqualifies) will get low score. 

# inspiration of RansackAI
In London University/London Business School's course "Managing company of the future" https://www.london.ac.uk/study/courses/moocs/managing-company-future, one of the lessons covered "Rank and Sack" strategy. This "Rank and Sack" strategy was used to wean out poor performers (untalented, analogous to parasites in host-parasite relationship of biology) and allow the high performers (talented, analogous to host in host-parasite relationship of biology) to work, ensuring success of the organization. I also observed that for the untalented/poor performer/parasites, their ability to control/blackmail the talented/high performer/host is by ganging up in decision making positons of hiring/firing and funding decisions. The untalented/poor performer/deceitful/parasites, exert control/blackmail by witholding jobs, education, funding and even publication (through bad reviews) from the talented/high performer/host. The deal is, the untalented/poor performer/deceitful/parasites gang up to kick out the talented/high performer/host, unless the talented/high performer/host gives the untalented/poor performer/deceitful/parasites free authorships/credit and remain subservient/silent to their lies/bullying/injustice. AI can take this blackmailing bullying power away from untalented/poor performer/deceitful/parasites to protect talented/high performer/host and enable talent (a goal of FAIRwissenschaft). Also by giving poor reference letters, scapegoating and sabotaging the work of talented/high performers the untalented/poor performer/deceitful/parasites exert control/blackmail power --- this can be easily fixed, if organizations ask for HR reference letters instead of PI/boss letters but this is beyond the scope of this RansackAI project. The term "Ransack" means rigorous search and also is a combination of Rank and Sack terms, so both ways this name seems like a good fit for this project. AI will take control/blackmailing power away from untalented/poor performer/deceitful/parasites and help pick the talented/high performers. This is good for AI future as only talented individuals who are constant learners and appliers of knowledge can keep up with AI, and manage AI. This is essential for a sustainable organization and in turn for a sustainable future of planet Earth that is driven by values of “Lernbegierde”, “Problemlösungswunsch” and “Endziele”. The fantastic 123 are the inspiration for this RansackAI project. To quote from Justice League movie, "they said the age of the heroes will not come again...but it has come, it must come again...to save the world". The fantastic 123, represent an "age of heroes". The world needs to know and apply using AI the fantastic 123's work now more than ever, if we are to save our planet. This RansackAI project, and other FAIRwissenschaft projects are steps towards this goal.  

############################################################################################################################
# INPUT (DATA SOURCE)
############################################################################################################################
######University/Institute/Organization employee (boss/PI and staff profile) and scientific literature. Results of poor scoring untalented/poor performer/deceitful/parasites and talented/high performer/host will be published in this repository and HAL repository.######

############################################################################################################################
# OUTPUT (DATA SOURCE)
############################################################################################################################
######Mock result showing format of output, in 'output_tmp' folder, mock file 'ransackai_mock_result.csv' (also excel format 'ransackai_mock_result.xlsx'). In simple words using generative AI a brief explanation will be added to make it explainable AI and also some back of the envelope formula or math explanation will be added (it woun't be as accurate as the AI calculated score but something approximate to make it more explainable). Author IDs such as Orcid will be used to identify authors so that incase of name change author can still be tracked and scored. Ranks will be both statistical ranks and also ML model predicted ranks. ######

############################################################################################################################
# SETUP
############################################################################################################################
#####Install Anaconda on your Mac or Linux Operating System#####
https://docs.anaconda.com/anaconda/install/index.html
######Create conda environment for R and IDE RStudio#####
#here env name being created is 'ransackai', please use any name you like
conda create --clone base --name ransackai
conda activate ransackai
#Type following to exit conda env
conda deactivate

############################################################################################################################
# USE
############################################################################################################################
######Commandline Usage######

######IDE Usage######

############################################################################################################################
# THANK YOU
############################################################################################################################

# Copyright ©2023-2024 Shradha Mukherjee. All rights reserved.  
